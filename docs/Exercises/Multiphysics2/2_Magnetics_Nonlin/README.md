# HW2: Nonlinear Magentic


### Description
Consider an axi-symmetric model of a coil and a plate as depicted in the figure.
The inner and outer radii of the coil, $r_1$ and $r_2$ , are 5 mm and 10 mm, respectively.
The length of the coil, $l_1$ , is 8 mm. The plate thickness, $l_2$ , is 3 mm.
The coil has 200 turns and is fed by a constant current of $I$ = 20 A. The plate is made out of iron. 
Iron in general exhibits a non-linear magnetic behavior, which is assumed to be defined by
\begin{equation}
B(H) = \frac{H}{c_1 H + c_2} + \mu_0H \, ,
\end{equation}
where $B$ and $H$ denote the magnetic flux density and field strength, respectively, and $\mu_0$ the vacuum permeability.
The parameters are given by $c_1$ = 0.56 Am/N and $c_2$ = 370 $\rm{A^2/N}$.
Tabulated values of the given function are included in the template files (bh.fnc).
For the air domain and the coil the free space permeability can be assumed.

In a first step consider only linearized material behavior and compare the cases with air and iron core. Then, specify a non-linear permeability for the iron core and compare to the linearized case.

![sketch](sketch.png){: style="width:200px"}

---------------------------

### Tasks
1. Create a regular hexaedral mesh using Trelis. **(2 Points)**
2. Create input-xml files for the required CFS++ simulations. **(2 Points)**
3. Document your results by answering the questions below. **(8 Points)**
4. Answer the theoretical question. **(3 Points)**

### Hints
- Use the provided templates [`coil.xml`](coil.xml) and [`mat.xml`](mat.xml). 
- Adapt the Trelis input [`geometry-ue.jou`](geometry-ue.jou) to include the ring. 
- Look into the \*.info.xml to get information about non-linear convergence.
- The workflow for the example is included in the shell script [`run.sh`](run.sh). Before you can successfully run it, you have to:
    - complete the trelis input [`geometry-ue.jou`](geometry-ue.jou) (choose a appropriate mesh size)
    - complete the CFS input [`coil.xml`](coil.xml)
    - delete the "#" symbol in [`run.sh`](run.sh) in the necessary lines to execute cfs and trelis (`#trelis...` and `#cfs...`)


### Submission
Submit all your input files (`*.jou`, `*.xml`) as well as a concise PDF report of your results. Name the archive according to the format `HWn_eNNNNNNN_Lastname.zip`.

---------------------------

### Material Behavior 
Plot the given material behavior $B ( H )$ for values up to 10 kA/m. Evaluate $\frac{\rm d B}{\rm d H}(0)= \mu_{\rm lin}$ to obtain the linearized material behavior for iron. Also include the linear approximation and the tabulated values (from file [`bh.fnc`](bh.fnc)) in the plot. **(3 Points)**

### Linear versus non-linear iron core
First start with a linear simulation.

- Plot the magnitude of the magnetic flux density as well as the vectors **(2 Points)**

After that, perform a nonlinear analysis. Therefore, use the tabulated data to define the input for a non-linear permeability in the iron core.

- How many non-linear iterations were done to solve the problem? **(1 Point)**
- Compare the magnitude of the magnetic flux density between linear and nonlinear result.
What do you observe? Is the linear approximation good? **(2 Point)**

### Theoretical part
How does coil modeling work in the 2D plane and axisymmetric case? How does the boundary conditions look like?

---------------------------

You can download the templates linked on this page individually by _right click --> save target as_.
Alternativey, head over to the [git repository](https://gitlab.com/openCFS/userdocu/-/tree/master/docs/Exercises/Multiphysics2) and use the _download button_ on the top right: 
Here is a direct link to the [__zip archive__](https://gitlab.com/openCFS/userdocu/-/archive/master/HW2.zip?path=docs/Exercises/Multiphysics2/2_Magnetics_Nonlin).

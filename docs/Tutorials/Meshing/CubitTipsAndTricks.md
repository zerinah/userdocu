
# Advanced tips and tricks

A lot of advanced tips and tricks like keeping track of IDs and using loops have already been covered in the example. This section serves as a quick reference guide where the most important features are listed again - for their application please refer to the example above.

## Get the ID of the last volume: 
```
Id("Volume")
```

* Loop for a certain number of times: 
```
#{Loop(10)}
    ...
#{EndLoop}
```

## If-statemet: 
```
#{If(uu>=7)}
    ...
#{EndIf}
```

## Get surfaces of a certain volume (range):
```
surface in volume {ID}
```

## Selection based on geometry:
```
with z_coord={z_val}
with x_max>={x_val}
with y_min<{y_val}
```

## General selection of volumes (and surfaces etc.):
```
all
except
and
```

## Use Python inside of Cubit
You can also use python inside cubit for scripting.
Just activate the script-tab under _Tools_ -> _Options_ -> _Layout_ -> _Show Python Tab_

Now you can use python inside cubit:
```
cubit.cmd("reset")
xList = [1,2,3,4,5]
for x in xList:
  cubit.cmd(f"brick x {x} y {x} z {x}")
  cubit.cmd(f"move volume {x} x {x}")
cubit.cmd(f"unite volume all except 1")
cubit.cmd(f"subtract volume 1 from 2")
```
## Use Cubit inside of Python-Editor
You can also use your favourite Code-Editor (like VSCode) but here you need to import and initialize the cubit library first:

```
import sys 
#path to cubit binary
sys.path.append("/path/to/Coreform-Cubit-2020.2/bin")
import cubit
#Without this init-command, i get an segfault
cubit.init(["cubit","-nojournal"])
```

Please note that the `cubit.init("cubit")` is critically, otherwise you can get a segfault.

## Export degenerate elements
Since blocks can only contain one type of element, the only option to export multiple element types in one block is to write them as a degenarte version of the "main" element type - if possible (e.g. write a wedge as a hex element).
Attention: This feature is not activated by default, hence exporting a block with multiple element types will result in holes! If you have strange problems with your simulation, check if this might be the cause!
To activate this feature just use 
```
Set Block Mixed Element Output Degenerate
```
somewhere before you export the mesh.

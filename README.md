# openCFS userdocu

This is the source code repository of the user documentation for [openCFS](https://opencfs.org).

The documentation is built from the [userdocu source repository](https://gitlab.com/openCFS/userdocu) via [MkDocs](https://www.mkdocs.org/)
and aims to provide a starters guite to openCFS.
It is automatically built by the [gitlab CI pipeline](.gitlab-ci.yml) and finally hosted via gitlab pages on [opencfs.gitlab.io/userdocu](https://opencfs.gitlab.io/userdocu).

The documentation currently only covers a small sub-set of the functionality of openCFS.
**We're happy about contributions to the documentation** to keep the documentation growing and up to date!

To edit the documentation you need

* an installation of [MkDocs](https://www.mkdocs.org/) including some extra packages
* a text editor, preferably with markdown support
* a web browser to view your changes locally

for details see the [contribution guide](CONTRIBUTING.md). If you have any questions, open an [issue](https://gitlab.com/openCFS/userdocu/-/issues).. 

Related projects
----------------

* [openCFS source code](https://gitlab.com/openCFS/cfs)
* [openCFS Testsuite](https://gitlab.com/openCFS/Testsuite)
* [openCFS developer wiki](https://gitlab.com/openCFS/cfs/-/wikis/home)
